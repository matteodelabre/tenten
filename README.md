Compilation :

Afin de faciliter le processus de compilation au cours du développement du
jeu, nous avons décidé d’employer le logiciel CMake qui génère automatique-
ment un Makefile en fonction des différents fichiers. Il est nécessaire d’installer
cet outil si ce n’est pas déjà fait.

Pour les distributions basés sur Debian, il faut exécuter la commande suivante:
sudo apt-get install build-essential cmake
Pour macOS:
En ligne de commande:
brew install cmake
(nécessite le gestionnaire de paquet Homebrew)
Téléchargement depuis le site internet:
https://cmake.org/download/
Il est également nécessaire d’installer la bibliothèque ncurses si ce n’est pas
déjà fait. Pour les distributions basés sur Debian, il faut exécuter la commande
suivante :
sudo apt install libncurses5-dev
Pour la génération du fichier Make, une fois placé dans le dossier ten-
ten/build, il faut exécuter la commande suivante dans le terminal :
cmake ..
Il est maitenant possible de compiler en appelant simplement la commande
make (toujours dans le dossier tenten/build) :
make
Une fois compiler nous pouvons lancer le jeu via
./tenten
(toujours dans le
dossier tenten/build).
Divers arguments peuvent être passés :
•
-h ou –-help pour afficher l’aide
•
-v ou –-version pour afficher la version
•
-l ou –-load suivi du nom du fichier de sauvegarde ("test" par défaut, sans l'extension ".tenten") afin de reprendre un partie précédemment sauvegardée


Ensuite nous pouvons jouer: L’objectif du jeu est donc de poser un maximum
de pièces afin de récolter le plus de point.  A l’aide des flêches directives du
clavier, on choisit la pièce à placer, on appuie sur entrée pour valider cette
pièce,on choisit l’emplacement où l’on veut placer la pièce (qui n’est pas déjà
pris) et on appuie sur entrée pour valider l’emplacement. Lorsqu’on remplit une
ou plusieurs ligne(s) ou colonne(s), on cumule des points et lorsque aucune des
pièces affichées n’est plaçable, le jeu s’arrête.
Le jeu dispose de plusieurs raccourcis clavier :
•
Ctrl + Q ou Ctrl + C pour quitter le pogramme
•
Ctrl + Z pour annuler l’action précédente
•
Ctrl + R pour recommencer le jeu
•
Ctrl + S pour sauvegarder

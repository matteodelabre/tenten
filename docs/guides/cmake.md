# Informations pratiques sur CMake

CMake est un générateur de fichiers de configuration pour les utilitaires de compilation du type Make. Il est multi-plateformes et peut également générer des configurations pour Visual Studio, Xcode, Code::Blocks, …

## Installation

L’installation dépend du système utilisé. Dans tous les cas, il faut s’assurer que la version soit au moins la 3.1 (la version peut être vérifiée avec la commande `cmake --version`). [Voir le site de CMake.](https://cmake.org/download/)

**Sur Ubuntu :** `sudo apt-get install cmake`.

## Compilation

Chaque fichier source ajouté ou supprimé doit être répercuté dans la liste des sources dans le fichier `CMakeLists.txt`.

Pour la première compilation :

```sh
cd build
cmake ..
make
```

Cela génère dans le dossier `build` un Makefile, entre autres, puis appelle `make` pour compiler le projet.

Pour les compilations suivantes, _inutile de rappeler CMake_ car `make` s’en occupera au besoin.

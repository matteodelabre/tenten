# Commandes utiles pour Git

Commande                                    | Commentaire
--------------------------------------------|---------------------------------
`git clone https://chemin.du.repo.example/` | Copie le dépôt donné depuis le serveur vers votre machine
`git add fichier1 dossier2`                 | Ajoute les fichiers donnés au prochain commit
`git add --all`                             | Ajoute tous les fichiers modifiés au prochain commit
`git reset HEAD fichier1 dossier2`          | Enlève les fichiers donnés du prochain commit
`git status`                                | Affiche tous les fichiers modifiés depuis le dernier commit
`git commit`                                | Valide le commit (ouvre un éditeur pour faire le message du commit)
`git commit --amend`                        | Valide le commit en le fusionnant avec le précédent
`git push`                                  | Envoie tous les commits locaux vers le serveur
`git pull`                                  | Récupère tous les commits en retard depuis le serveur
`git blame fichier`                         | Voir qui est responsable de quelles portions d'un fichier
`git log --all --graph --decorate --oneline`| Résumé des derniers commits

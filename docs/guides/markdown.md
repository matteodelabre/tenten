# Syntaxe importante pour Markdown

Dans le fichier source      | Effet sur le rendu
----------------------------|---------------------------------
Sauter une ligne            | Nouveau paragraphe
`*Italique*`                | *Italique*
`**Gras**`                  | **Gras**
`# Titre principal`         | Titre principal
`## Sous-titre`             | Sous-titre
`### Sous-sous-titre`       | Sous-sous-titre
`- Liste`                   | Liste non-ordonnée
`1. Liste`                  | Liste ordonnée
`* * *`                     | Séparateur horizontal
`[Légende](http://lien)`    | [Légende](http://lien)
\`Code source\`             | `Code source`

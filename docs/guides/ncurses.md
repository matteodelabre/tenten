# Utilisation de ncurses

_Basé sur [NCURSES Programming HOWTO](http://tldp.org/HOWTO/NCURSES-Programming-HOWTO) par Pradeep Padala, et les pages de manuel de ncurses._

La bibliothèque ncurses permet de faire des interfaces graphiques complexes dans le terminal de façon plus gérable qu’à coup de simples `std::cout`. Pour utiliser la bibliothèque, il faut inclure l’en-tête `<ncurses.h>` et lier le code à `libncurses`.

## Initialisation et fin de programme

Ces fonctions ne renvoient rien. Elles permettent d’initialiser ou de configurer la bibliothèque.

Fonction               | Description
-----------------------|-----------------------------------
`initscr()`            | Démarre le mode ncurses et initialise la bibliothèque. À appeler en début de programme.
`endwin()`             | Termine le mode ncurses et rétablit le terminal dans son état normal. À appeler en fin de programme.
`noecho()`             | N’affiche pas ce que tape l’utilisateur sur le terminal (comme s’il tapait un mot de passe).
`cbreak()`             | Passe directement les touches enfoncées par l’utilisateur au programme, sans attendre un retour à la ligne.
`raw()`                | Comme `cbreak()`, mais passe également les caractères de contrôle comme Ctrl+C et Ctrl+Z directement au programme. **Ce qui fait qu’appuyer sur Ctrl+C ne mettra pas fin au programme : c’est au programme de gérer son arrêt.**
`keypad(stdscr, true)` | Active le passage de touches comme les flèches directionnelles au programme.
`curs_set(X)`          | Change la visibilité du curseur : si `X = 0`, curseur caché, si `X = 1`, visibilité normale, si `X = 2`, visibilité renforcée.

## Affichage

ncurses utilise un système de fenêtres (window) pour découper le terminal en zones. Ces zones sont uniquement conceptuelles, elles ne sont pas matérialisées sur le terminal (pas de bordures, par exemple).

Une fenêtre par défaut, appelée `stdscr`, occupe la totalité de l’écran. Les fonctions d’affichage qui ne commencent pas par `w` affectent la fenêtre par défaut. Leur équivalent préfixé de `w` affecte la fenêtre passée en argument.

Chaque fenêtre contient un curseur virtuel, différent de celui de l’écran, qui bouge à chaque affichage. Les fonctions qui ne commencent pas par `mv` effectuent l’affichage à la position actuelle du curseur dans la fenêtre concernée. Leur équivalent préfixé de `mv` déplace le curseur à la position donnée avant de s’appliquer.

Enfin, les fonctions d’affichage ncurses n’affichent rien dans le terminal tant que les fonctions de rafraîchissement ne sont pas appelées (voir la partie sur le rafraîchissement). Cela permet de préparer des affichages complexes et de les afficher en un seul coup. ncurses est optimisé pour faire le moins d’affichages possible, car l’affichage prend du temps.

*NB1 :* le préfixe `mv` vient toujours avant le préfixe `w`.  
*NB2 :* dans toutes les fonctions de ncurses, **on met l’ordonnée avant l’abscisse (y avant x).**

### Affichage formatté

Ces fonctions permettent de faire de l’affichage à la `printf`, mais dans des fenêtres ncurses. Chaque appel déplace le curseur de la fenêtre concernée à la fin du message affiché.

Fonction                               | Description
---------------------------------------|---------------------
`printw(format, arg, …)`               | S’utilise comme `printf`, affiche le message à la position actuelle du curseur dans la fenêtre par défaut.
`mvprintw(y, x, format, arg, …)`       | Comme `printw`, mais déplace le curseur à la position donnée (x, y) avant d’afficher le message.
`wprintw(win, format, arg, …)`         | Comme `printw`, mais s’applique à la fenêtre passée en premier argument.
`mvwprintw(win, y, x, format, arg, …)` | Comme `mvprintw`, mais s’applique à la fenêtre passée en premier argument.

Exemple d’utilisation :

```cpp
// Affiche Bonjour ! dans la fenêtre principale
printw("Bonjour !");

// Affiche 5 + 3 = 8 en position 10x4
mvprintw(4, 10, "%d + %d = %d", 5, 3, 5 + 3);
```

### Affichage d’un seul caractère

Ces fonctions permettent d’afficher un seul caractère dans la fenêtre concernée et d’avancer d’un cran vers la droite. Elles permettent de définir directement les attributs du caractère affiché (voir la section sur les attributs juste ci-dessous).

Fonction                   | Description
---------------------------|---------------------
`addch(car)`               | Affiche le caractère donné sur la fenêtre par défaut à la position actuelle du curseur. Voir la section suivante pour les attributs.
`mvaddch(y, x, car)`       | Comme `addch`, mais déplace le curseur à la position donnée (x, y) avant d’afficher le caractère.
`waddch(win, car)`         | Comme `addch`, mais s’applique à la fenêtre passée en premier argument.
`mvwaddch(win, y, x, car)` | Comme `mvaddch`, mais s’applique à la fenêtre passée en premier argument.

Exemples d’utilisation :

```cpp
// Affiche un 'a' à la position actuelle de la fenêtre par défaut
addch('a');

// Affiche un 'x' en 5x10 de la fenêtre test
mvwaddch(test, 10, 5, 'x');

// Équivalent au précédent
wmove(test, 10, 5);
waddch(test, 'x');
```

### Attributs d’affichage

Les attributs permettent d’ajouter des effets de format sur l’affichage. Ils se présentent sous la forme de constantes agissant comme des drapeaux _(flags)_ pouvant être combinés.

Voici quelques attributs utiles. D’autres attributs plus ou moins importants existent. Des attributs de couleur peuvent également être définis, ils sont décrits plus en détail dans la section sur les couleurs.

Constante      | Effet                                                 | Aperçu
---------------|-------------------------------------------------------|-------
`A_NORMAL`     | Affichage normal.                                     | ![Effet de A_NORMAL](images/attr-normal.png)
`A_STANDOUT`   | Surlignage.                                           | ![Effet de A_STANDOUT](images/attr-standout.png)
`A_UNDERLINE`  | Soulignage.                                           | ![Effet de A_UNDERLINE](images/attr-underline.png)
`A_REVERSE`    | Inverse le fond et la couleur du texte.               | ![Effet de A_REVERSE](images/attr-reverse.png)
`A_BLINK`      | Clignotant (ne fait rien sur la plupart des terminaux).| ![Effet de A_BLINK](images/attr-blink.png)
`A_DIM`        | Moins lumineux.                                       | ![Effet de A_DIM](images/attr-dim.png)
`A_BOLD`       | Plus lumineux/gras.                                   | ![Effet de A_BOLD](images/attr-bold.png)
`A_INVIS`      | Invisible.                                            | ![Effet de A_INVIS](images/attr-invis.png)

Pour combiner plusieurs attributs, on utilise l’opérateur OU (|). Par exemple, `A_UNDERLINE | A_BOLD` donnera du gras souligné. Pour activer ou désactiver les attributs, on utilise les fonctions suivantes.

Fonction              | Description
----------------------|---------------------
`attron(attr)`        | Active les attributs donnés sur la fenêtre par défaut.
`wattron(win, attr)`  | Comme `attron`, mais sur la fenêtre donnée.
`attroff(attr)`       | Désactive les attributs donnés sur la fenêtre par défaut.
`wattroff(win, attr)` | Comme `attroff`, mais sur la fenêtre donnée.

Les appels à ces fonctions sont cumulatifs. Ainsi, appeler `attron(A_UNDERLINE)` suivi de `attron(A_BOLD)` est équivalent à appeler `attron(A_UNDERLINE | A_BOLD)`. Pour remplacer les attributs actuels au lieu de les cumuler, il faut utiliser les fonctions suivantes.

Fonction               | Description
-----------------------|---------------------
`attrset(attr)`        | Définit les attributs donnés sur la fenêtre par défaut.
`wattrset(win, attr)`  | Comme `attrset`, mais sur la fenêtre donnée.

Lors de l’utilisation de `addch`, il est possible de donner directement les attributs du caractère à afficher en combinant les drapeaux avec le caractère.

Exemples d’utilisation :

```cpp
// Affiche en gras souligné dans la fenêtre win
wattron(win, A_UNDERLINE | A_BOLD);
wprintw(win, "Il fait beau aujourd'hui.");

// Réinitialise les attributs de win (remplacement par A_NORMAL)
wattrset(win, A_NORMAL);

// Ajoute un 'a' souligné dans la fenêtre principale
addch('a' | A_UNDERLINE);
```

### Rafraîchissement

Une fois l’affichage préparé en mémoire, il faut demander à ncurses d’appliquer cet affichage sur le terminal. Si des fenêtres se chevauchent, la dernière à avoir été rafraîchie apparaîtra sur la zone de chevauchement.

Fonction        | Description
----------------|---------------------
`refresh()`     | Rafraîchit la fenêtre principale.
`wrefresh(win)` | Rafraîchit la fenêtre donnée.

## Couleurs

L’utilisation des couleurs dans un programme ncurses passe par deux étapes. Il faut d’abord *initialiser les paires* de couleur. Une paire est une combinaison d’une couleur de fond (celle derrière les caractères du terminal) et d’une couleur de texte.

Fonction               | Description
-----------------------|-----------------------------------
`start_color()`              | Active le mode couleur de ncurses. À appeler juste après `initscr`.
`init_pair(id, texte, fond)` | Initialise la paire de couleur n°`id` pour correspondre à la combinaison de fond et de texte données. La paire n°0 est spéciale et dénote l’absence de couleur. Elle ne peut pas être modifiée.

Une fois les paires initialisées, elles sont accessibles en tant via la macro `COLOR_PAIR(x)`, `x` étant le numéro de la paire à utiliser. Cette macro se comporte comme un attribut d’affichage comme vu ci-dessus.

La liste des couleurs pouvant être appairées est prédéfinie. Certains terminaux permettent de modifier leur teneur en rouge, vert et bleu, mais il est mieux de se tenir aux couleurs suivantes pour avoir la meilleure portabilité.

* `COLOR_BLACK`
* `COLOR_RED`
* `COLOR_GREEN`
* `COLOR_YELLOW`
* `COLOR_BLUE`
* `COLOR_MAGENTA`
* `COLOR_CYAN`
* `COLOR_WHITE`

Exemple d’utilisation :

```cpp
// Affiche un message en gras rouge
initscr();
start_color();

init_pair(1, COLOR_RED, COLOR_BLACK);

attron(A_BOLD | COLOR_PAIR(1));
wprint("Je vois la vie en rouge.");
```

## Gestion des fenêtres

Les fenêtres ncurses sont juste une abstraction et ne sont pas matérialisées dans le terminal, mais elles permettent de simplifier le code en évitant notamment de décupler les déplacements de curseur.

### Création et suppression

Ces fonctions permettent de gérer les `WINDOW*` qui représentent une fenêtre ncurses.

Fonction                     | Description
-----------------------------|---------------------
`WINDOW* newwin(h, w, y, x)` | Crée une nouvelle fenêtre dont le coin supérieur gauche est à (x, y) et dont la taille est (w, h). Si `h` vaut zéro, la fenêtre prend toute la hauteur de l’écran (constante LINES). Si `w` vaut zéro, la fenêtre prend toute la largeur de l’écran (constante WIDTH). **La fenêtre n’a pas le droit de sortir du terminal.**
`mvwin(win, y, x)`           | Déplace le coin supérieur gauche de la fenêtre donnée en position (x, y). La fenêtre n’a toujours pas le droit de sortir du terminal.
`wresize(win, h, w)`         | Redimensionne la fenêtre donnée pour occuper la taille (w, h). Là aussi, il faut s’assurer que la fenêtre ne sorte pas du terminal.
`delwin(win)`                | Libère les ressources de la fenêtre mais n’enlève pas son image du terminal.

### Effacement

Fonction                     | Description
-----------------------------|---------------------
`erase()`                    | Efface le contenu de la fenêtre par défaut.
`werase(win)`                | Efface le contenu de la fenêtre donnée.

### Décorations

Même si les fenêtres ne sont pas matérialisées, elles peuvent être dotées d’un fond et de bordures assez simplement. Ces décorations font cependant partie du contenu de la fenêtre.

Fonction                                   | Description
-------------------------------------------|---------------------
`border(l, r, t, b, tl, tr, bl, br)`       | Dessine une bordure collée aux extrémités de la fenêtre par défaut. Les arguments donnent les caractères utilisés pour chaque partie de la bordure (respectivement la gauche, la droite, le haut, le bas, le coin haut-gauche, le coin haut-droite, le coin bas-gauche et le coin bas-droite). Si ces arguments sont mis à zéro, un caractère par défaut est utilisé.
`wborder(win, l, r, t, b, tl, tr, bl, br)` | Comme `border`, mais s’applique à la fenêtre passée en argument.
`bkgd(attr)`                               | Modifie le fond de la fenêtre et met à jour le contenu de la fenêtre en conséquence (s’il y avait déjà un fond, celui-ci est remplacé). Le fond est passé sous forme d’attribut (tel que décrit ci-dessus).
`wbkgd(win, attr)`                         | Comme `bkgd`, mais s’applique à la fenêtre passée en argument.

### Curseur

Chaque fenêtre est dotée de son curseur virtuel, utile pour les affichages complexes. Ces fonctions permettent de déplacer ces curseurs virtuels. Les fonctions d’affichage préfixées de `mv` déplacent le curseur avant de s’appliquer elles-mêmes : il s’agit juste de raccourcis vers les fonctions suivantes.

Fonction           | Description
-------------------|---------------------
`move(y, x)`       | Déplace le curseur de la fenêtre principale en (x, y).
`wmove(win, y, x)` | Déplace le curseur de la fenêtre donnée en (x, y).

## Interaction avec le clavier

La fonction `getch()` permet de récupérer un caractère, au sens large. Si l’utilisateur n’a rien saisi, elle bloque le programme jusqu’à ce qu’il le fasse. Elle peut renvoyer aussi bien des caractères imprimables (chiffres, lettres, …) que des caractères spéciaux (touches directionnelles, F1–F12, échap, …).

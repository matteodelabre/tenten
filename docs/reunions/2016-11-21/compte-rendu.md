# Réunion du 21/11/16

Ordre du jour : réunion initiale de constitution du groupe de travail.

* Création du dépôt GitHub
* Détermination des structures de données en accord avec le
  cahier des charges
* Répartition des tâches pour l'implémentation des algos initiaux
* Représentation U.M.L.

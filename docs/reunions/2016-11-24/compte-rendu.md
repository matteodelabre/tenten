# Réunion du 24/11/16

Ordre du jour : choix des outils de travail et organisation du projet dans le temps.

## Outils

* Atom et Emacs
* Git et GitHub
* CMake

* Style de code :
    * 4 espaces
    * Style d'indentation Allman
    * Style de nommage :
        * camel case supérieur pour les classes (MaClasse)
        * camel case inférieur pour les fonctions (maFonction)
        * autre pour les variables (ma_variable)
        * attributs privés précédés de _
        * PAS DE CODE dans les en-têtes
        * accesseurs précédés de set ou get
    * Commentaires en français et noms de symboles en anglais
    * Commentaires en Doxygen

## Organisation du projet

Écriture des en-têtes des classes :

* `TenTen`, `EtatPartie` : Rémi
* `Grille` : Camille
* `Partie` : Jérémie
* `EtatCellule`, `Pièce` : Mattéo

Création de la structure des dossiers.

# Réunion du 04/12/16

Ordre du jour : répartition des tâches concernant l'implantation des différentes fonctions du jeu.

## Répartition

* Méthodes `isPossible` et `isAllowed` de la classe `Grille` : Jérémie
* Méthodes `next` et `previous` de la classe `Game` ainsi que ses différents constructeurs et accesseurs : Mattéo
* Méthodes `placePiece` et `removeLines` de la classe `Grille` : Camille
* Autres accesseurs et constructeurs : Rémi

Ainsi que la réalisation d'un fichier `cmake` élémentaire permettant la compilation.

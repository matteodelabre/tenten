# Réunion du 15/12/16

Ordre du jour : revue du travail effectué par chacun depuis la dernière réunion et travail collectif.

## Revue du travail

* Ajout d'une méthode `at()` dans Grid pour simplifier l'écriture.
* Correction de l'algorithme `placePiece`.

## Objectifs pour la prochaine séance

* Terminer les algorithmes `isPossible` et `isAllowed`.

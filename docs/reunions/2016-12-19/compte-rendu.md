# Réunion du 19/12/16

## Revue des algos `isPossible` et `isAllowed`

Adoption d’un algo en _force brute_, au moins temporairement.

## Répartition du travail

Distinction de trois phases de travail :

1. Modélisation, structure et cœur algorithmique
2. Tests et affichage primitifs
3. Interface finale (ncurses)

Première phase terminée ce jour.

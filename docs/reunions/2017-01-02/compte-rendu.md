# Réunion du 02/01/17

Ordre du jour : vers un premier affichage. Distribuer le travail restant concernant l’affichage, les tests et le rapport.

## À faire

* **Mattéo :** écrire un constructeur utile pour `Grid` qui doit permettre d’initialiser une grille ainsi :

```c
Grid grid(std::size_t width, std::string format);
```

Exemple d’utilisation :

```c
Grid grid(2, "R "
             "RR"
             " R");
```

La chaîne `format` décrit le contenu de la grille. Une espace code une case vide. On code le rouge, le vert, le bleu, le jaune et le noir respectivement par R, G, B, Y et K.

* **Jérémie :** utiliser le constructeur de `Grid` pour implanter les formes de pièces prédéfinies dans le sujet, dans la classe `Game`.

* **Rémi :** créer une méthode `Grid::print(std::ostream)` pour afficher le contenu d’une grille vers le flux en paramètre.

* **Camille :** créer une méthode `Game::play()` pour lancer la boucle d’un jeu. Tant que le jeu n’est pas fini, faire :
    1. Si aucun coup n’est possible alors le jeu est fini.
    2. Sinon, afficher la grille actuelle et les pièces à placer.
    3. Faire choisir une pièce à placer.
    4. Faire choisir les coordonnées où placer.
    5. Tenter de placer la pièce choisie à l’endroit choisi.
    6. Si ce n’est pas possible, afficher un message d’erreur.

* Commencer le rapport (structuration LaTeX, diagrammes, …).

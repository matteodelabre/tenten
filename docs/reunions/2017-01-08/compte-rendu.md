# Réunion du 08/01/17

## Affichage avec `ncurses`

Mattéo, Jérémie.

1. Dessiner et prévoir l’interface.
2. Implanter l’interface dans `Game`.

## Sauvegarde et chargement

Camille, Rémi.

1. Sauvegarde des meilleurs scores dans un fichier.
2. Formalisme pour les pièces et la pile d’états.
3. Sauvegarde et chargement des parties dans des fichiers.

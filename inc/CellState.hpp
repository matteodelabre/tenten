/**
 * @file
 * @brief Définit l'énumération @c CellState et les fonctions associées.
 */

#include <string>

#ifndef __TENTEN_CELL_STATE_HPP_INCLUDED__
#define __TENTEN_CELL_STATE_HPP_INCLUDED__

/**
 * @brief Énumère les différents états possibles pour une cellule
 * d'une grille @c Grid.
 */
enum class CellState
{
    EMPTY,
    RED,
    GREEN,
    BLUE,
    YELLOW,
    BLACK
};

/**
 * @brief Convertit l'état d'une cellule en caractère coloré qui peut servir
 * à représenter cette cellule sur l’écran.

 * @param state L’état de la cellule en question.
 * @return Caractère coloré correspondant à l'état.
 */
int stateToColor(CellState state);

/**
 * @brief Convertit l'état d'une cellule en un code qui peut servir à
 * représenter cette cellule en format texte.

 * @param state L’état de la cellule en question.
 * @return Caractère correspondant à l’état.
 */
 char stateToChar(CellState state);

 /**
  * @brief Convertit la représentation d'une cellule en format texte en
  * état d'une cellule.

  * @param character L’état de la cellule sous forme de texte.
  * @return state L’état de la cellule en question.
  */
  CellState charToState(char character);

#endif // __TENTEN_CELL_STATE_HPP_INCLUDED__

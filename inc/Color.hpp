/**
 * @file
 * @brief Définit l'énumération @c Color et les fonctions de gestion des
 * paires de couleurs.
 */

#ifndef __TENTEN_COLOR_HPP_INCLUDED__
#define __TENTEN_COLOR_HPP_INCLUDED__

/**
 * @brief Énumère les différentes paires de couleur utilisables.
 */
enum ColorPairs
{
    BG_RED = 1,
    BG_GREEN,
    BG_BLUE,
    BG_YELLOW,
    BG_WHITE,
    SELECTED
};

/**
 * @brief Initialise les paires de couleurs ncurses.
 */
void initColorPairs();

#endif // __TENTEN_COLOR_HPP_INCLUDED__

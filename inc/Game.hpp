/**
 * @file
 * @brief Définit la classe @c Game.
 */

#ifndef __TENTEN_GAME_HPP_INCLUDED__
#define __TENTEN_GAME_HPP_INCLUDED__

#include "Piece.hpp"
#include "GameState.hpp"
#include "GridView.hpp"
#include "Message.hpp"
#include "Scores.hpp"

#include <vector>
#include <stack>
#include <cstdlib>
#include <fstream>
#include <cstring>

/**
 * @brief Représente une instance du jeu.
 *
 * Chaque instance du jeu contient notamment un tableau avec toutes les
 * formes de pièces possibles et une pile des états précédents du jeu.
 */
class Game
{
public:
    /* ÉNUMÉRATIONS */

    /**
     * @brief Les différents états de sortie du jeu.
     */
    enum class Exit
    {
        END,
        QUIT
    };

    /* CONSTRUCTEURS */

    /**
     * @brief Construit une instance du jeu avec les paramètres par défaut.
     * @param scores Adresse de l’instance de gestion des scores
     */
    Game(Scores* scores);

    /* MÉTHODES */

    /**
     * @brief Passe à l'état du jeu suivant en plaçant, si le coup est valide,
     * la pièce actuellement sélectionnée (_current_piece) aux coordonnées
     * choisies (_current_x et _current_y).
     *
     * @return Vrai si le coup est valide, faux sinon. Si vrai, un nouvel état
     * de jeu est empilé.
     */
    bool next();

    /**
     * @brief Annule la dernière action.
     */
    void previous();

    /**
     * @brief Lance la boucle de jeu.
     * @return Une valeur indiquant l’action souhaitée par l’utilisateur.
     */
    Exit play();

    /* FICHIERS */

    /**
     * @brief Charge la partie depuis le fichier de sauvegarde de nom donné.
     *
     * @param name Nom du fichier depuis lequel écrire.
     */
    void read(std::string name);

    /**
     * @brief Sauvegarde la partie actuelle dans un fichier de nom donné.
     * Le fichier sera écrit dans le dossier save/.
     *
     * @param name Nom du fichier où écrire.
     */
    void write(std::string name) const;

private:
    /**
     * Rafraîchit l’affichage dans le terminal avec la configuration
     * actuelle du jeu
     */
    void print();

    /**
     * Choisit @p quantity pièces parmi les modèles dans
     * @p available_pieces
     *
     * @param quantity Quantité de pièces à choisir
     * @return Un tableau contenant les pièces choisies
     */
    std::vector<Piece> choosePieces(std::size_t quantity) const;

    /**
     * @brief Choisit la pièce suivante parmi les pièces non-placées.
     */
    void chooseNextPiece();

    /**
     * @brief Choisit la pièce précédente parmi les pièces non-placées.
     */
    void choosePreviousPiece();

    /**
     * @brief S’assure que la pièce actuellement sélectionnée est bien
     * une pièce non-placée, sinon, la modifie pour prendre une pièce
     * non-placée.
     */
    void checkCurrentPiece();

    /**
     * @brief Indice de la pièce actuellement sélectionnée dans le jeu.
     */
    std::size_t _current_piece;

    /**
     * @brief Abscisse où placer la pièce actuellement choisie.
     */
    std::size_t _current_x;

    /**
     * @brief Ordonnée où placer la pièce actuellement choisie.
     */
    std::size_t _current_y;

    /**
     * @brief L’utilisateur a-t-il confirmé le choix de la pièce ?
     */
    bool _piece_chosen;

    /**
     * @brief Liste des pièces parmi lesquelles on peut choisir les
     * pièces à placer.
     */
    std::vector<Piece> _available_pieces;

    /**
     * @brief Pile des états du jeu.
     */
    std::stack<GameState> _states;

    // Gestionnaire de scores
    Scores* _scores;

    // Visualiseurs de grilles
    GridView _main_gridview;
    std::vector<GridView> _pieces_gridviews;

    // Boîtes à texte
    Message _shortcuts;
    Message _score;
};

#endif // __TENTEN_GAME_HPP_INCLUDED__

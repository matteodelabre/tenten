/**
 * @file
 * @brief Définit la classe @c GameState.
 */

#ifndef __TENTEN_GAME_STATE_HPP_INCLUDED__
#define __TENTEN_GAME_STATE_HPP_INCLUDED__

#include "Piece.hpp"
#include "Grid.hpp"

#include <iostream>

/**
 * @brief Représente un état du jeu à un moment donné.
 *
 * Un état du jeu contient le score du joueur, la grille ainsi que les
 * différentes pièces du jeu à un moment donné.
 */
class GameState
{
public:
    /* CONSTRUCTEURS */

    /**
     * @brief Construit un état vierge du jeu.
     */
    GameState();

    /**
     * @brief Construit un état du jeu à partir de la grille,
     * des pièces restantes et du score donné.
     *
     * @param main_grid Grille à utiliser.
     * @param remaining_pieces Pièces restant à placer.
     * @param score Score actuel.
     */
    GameState(
        Grid _main_grid,
        std::vector<Piece> _remaining_pieces,
        unsigned int _score
    );

    /* ACCESSEURS */

    /**
     * @brief Récupère la grille de l'état.
     */
    const Grid& getMainGrid() const;

    /**
     * @brief Récupère les pièces restantes à placer de l'état.
     */
    const std::vector<Piece>& getRemainingPieces() const;

    /**
     * @brief Récupère le score de l'état.
     */
    const unsigned int& getScore() const;

    /* FICHIERS */

    /**
     * @brief Lit un état depuis le flux @p out.
     *
     * @param in Flux depuis lequel lire.
     */
    void read(std::istream& in);

    /**
     * @brief Écrit le contenu de cet état dans le flux @p out.
     *
     * @param out Flux dans lequel écrire de contenu.
     */
    void write(std::ostream& sortie) const;

private:
    /**
     * @brief Grille principale de 10x10 dans laquelle les pièces sont placées.
     */
    Grid _main_grid;

    /**
     * @brief Pièces restant à placer.
     */
    std::vector<Piece> _remaining_pieces;

    /**
     * @brief Score actuel.
     */
    unsigned int _score;
};

#endif // __TENTEN_GAME_STATE_HPP_INCLUDED__

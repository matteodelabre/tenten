/**
 * @file
 * @brief Définit la classe @c Grid.
 */

#ifndef __TENTEN_GRID_HPP_INCLUDED__
#define __TENTEN_GRID_HPP_INCLUDED__

#include <vector>
#include <string>
#include <cstdlib>
#include <ostream>
#include <fstream>


#include "CellState.hpp"

class Piece;

/**
 * @brief Représente les grilles dans le jeu, c'est-à-dire la grille principale
 * ou les grilles contenant les formes de pieces.
 *
 * Dans le jeu on a plusieurs grilles : la grille principale (fixée à 10x10)
 * où l'on place nos pieces de formes, definies par une nouvelle grille avec
 * des tailles variables. Des fonctions test regardent si les grilles des
 * @c Piece sont compatibles avec le jeu en cours.
 */
class Grid
{
public:
    /* CONSTRUCTEURS ET DESTRUCTEURS */

    /**
     * @brief Construit une grille par défaut de dimensions 10x10 et ne
     * contenant que des cases vides (jeu par défaut).
     */
    Grid();

    /**
     * @brief Construit une grille de dimensions données avec le
     * contenu donné.
     *
     * @code{.cpp}
     * Grid test_grid(2, "R "
     *                   "RR"
     *                   " R");
     * @endcode
     *
     * @param width Largeur de la grille (nombre de colonnes).
     * @param content Contenu de la grille sous forme de chaîne de caractères
     * contenant exclusivement :
     *  * une espace pour une case vide ;
     *  * un R pour une case rouge ;
     *  * un G pour une case verte ;
     *  * un B pour une case bleue ;
     *  * un Y pour une case jaune ;
     *  * un K pour une case noire.
     */
    Grid(std::size_t width, std::string content);

    /* ACCESSEURS */

    /**
     * @brief Récupère la largeur de la grille.
     */
    std::size_t getWidth() const;

    /**
     * @brief Récupère la hauteur de la grille.
     */
    std::size_t getHeight() const;

    /**
     * @brief Récupère l’état d’une cellule du contenu
     */
    const CellState& at(std::size_t x, std::size_t y) const;

    /* MÉTHODES */

    /**
     * @brief Calcule si l'une des pièces du tableau donné peut être
     * placée dans la grille ou non (au moins 1 combinaison possible).
     *
     * @param piece Tableau des pièces à vérifier.
     * @return Vrai si une des pièces est plaçable, faux sinon.
     */
    bool isPossible(const std::vector<Piece>& pieces) const;

    /**
     * @brief Calcule si la pièce donnée peut légalement être
     * placée aux coordonnées données.
     *
     * @param piece Pièce à vérifier.
     * @param x Abscisse à vérifier.
     * @param y Ordonnée à vérifier.
     * @return Vrai si la pièce peut être placée aux coordonées données.
    */
    bool isAllowed(const Piece& piece, std::size_t x, std::size_t y) const;

    /**
     * @brief Place une pièce aux coordonnées données.
     *
     * @param piece Pièce à placer.
     * @param x Abscisse où placer la pièce.
     * @param y Ordonnées où placer la pièce.
     */
    void placePiece(const Piece& piece, std::size_t x, std::size_t y);

    /**
     * @brief Nettoie les colonnes et les lignes pleines et renvoie le score
     * correspondant.
     *
     * @return Le nombre de points à ajouter au score pour récompenser les
     * lignes et colonnes vidées.
    */
    unsigned int clean();

    /* FICHIERS */

    /**
     * @brief Lit une grille depuis le flux @p out.
     *
     * @param in Flux depuis lequel lire.
     */
    void read(std::istream&);

    /**
     * @brief Écrit le contenu de la grille en format texte dans le flux @p out.
     *
     * @param out Flux dans lequel écrire le contenu.
     */
    void write(std::ostream& out) const;

private:
    /**
     * @brief Récupère ou modifie l’état d’une cellule du contenu
     */
    CellState& _at(std::size_t x, std::size_t y);

    /**
     * @brief Vérifie si une colonne ou une ligne est pleine
     *
     * @param index L'indice de la colonne ou ligne à vérifier
     * @param is_column S'il s'agit d'une colonne ou d'une ligne
     * @return Vrai si et seulement si la colonne ou la ligne est pleine
     */
    bool isFull(std::size_t index, bool is_column) const;

    /**
     * @brief Vide la colonne ou la ligne correspondante
     *
     * @param index L'indice de la colonne ou ligne à vider
     * @param is_column S'il s'agit d'une colonne ou d'une ligne
     */
    void cleanFull(std::size_t index, bool is_column);

    /**
     * @brief Largeur de la grille.
     */
    std::size_t _width;

    /**
     * @brief Longueur de la grille.
     */
    std::size_t _height;

    /**
     * @brief Tableau à deux dimensions contenant les états des cellules
     * de la grille.
     */
    std::vector<std::vector<CellState>> _content;


};

#endif // __TENTEN_GRID_HPP_INCLUDED__

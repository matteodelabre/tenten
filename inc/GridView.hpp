/**
 * @file
 * @brief Définit la classe @c GridView.
 */

#ifndef __TENTEN_GRIDVIEW_HPP_INCLUDED__
#define __TENTEN_GRIDVIEW_HPP_INCLUDED__

#include <ncurses.h>
#include <cstdlib>

#include "Grid.hpp"

/**
 * @brief Élément graphique permettant de visualiser le contenu d’une grille
 * de manière intuitive.
 */
class GridView
{
public:
    /* ÉNUMÉRATIONS */

    /**
     * @brief Liste des états d’affichage des grilles.
     */
    enum class State
    {
        NORMAL,
        DISABLED,
        SELECTED
    };

    /* CONSTRUCTEURS ET DESTRUCTEURS */

    /**
     * @brief Construit un visualiseur de grille pour la grille donnée.
     * @param grid La grille à afficher dans le visualiseur.
     */
    GridView(Grid grid);

    /**
     * @brief Copie le visualiseur de grille donné.
     */
    GridView(const GridView& src);

    /**
     * @brief Affecte le visualiseur actuel au visualiseur donné.
     */
    GridView& operator=(const GridView&);

    /**
     * @brief Détruit le visualiseur de grille.
     */
    ~GridView();

    /* ACCESSEURS */

    /**
     * @brief Change la grille à afficher dans le visualiseur.
     *
     * @param grid Nouvelle grille à afficher.
     */
    void setGrid(Grid grid);

    /* MÉTHODES */

    /**
     * @brief Nettoie la grille précédemment affichée.
     */
    void erase() const;

    /**
     * @brief Affiche la grille à l'écran aux coordonnées (@p wx, @p wy),
     * dans l’état @p state.
     *
     * @param wx Abscisse du coin supérieur gauche où afficher la grille.
     * @param wy Ordonnée du coin supérieur gauche où afficher la grille.
     * @param state État dans lequel afficher la grille.
     */
    void print(
        std::size_t wx, std::size_t wy,
        State state = State::NORMAL
    ) const;

private:
    Grid _grid;
    WINDOW* _gridwin;
};

#endif //__TENTEN_GRIDVIEW_HPP_INCLUDED__

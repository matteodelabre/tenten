/*
 * @file
 * @ brief Définit la classe @c Message.
 */

#ifndef __TENTEN_MESSAGE_HPP_INCLUDED__
#define __TENTEN_MESSAGE_HPP_INCLUDED__

#include <ncurses.h>
#include <string>

/**
 * @brief Élément graphique permettant d’afficher un message contenant
 * une information à destination du joueur comme le score actuel et le
 * meilleur score.
 */
class Message
{
public:
    /* CONSTRUCTEURS ET DESTRUCTEURS */

    /**
     * @brief Construit un message de largeur @p width et de hauteur
     * @p height.
     *
     * @param width Largeur de la boîte du message.
     * @param height Hauteur de la boîte du message (nombre de lignes de texte).
     */
    Message(unsigned int width, unsigned int height);

    /**
     * @brief Copie le message donné.
     */
    Message(const Message& src);

    /**
     * @brief Affecte le message actuel au message donné.
     */
    Message& operator=(const Message&);

    /**
     * @brief Détruit le message.
     */
    ~Message();

    /* MÉTHODES */

    /**
     * @brief Nettoie le message précédemment affiché.
     */
    void erase() const;

    /**
     * @brief Affiche le message à l’écran aux coordonées (@p wx, @p wy),
     * avec le contenu donné.
     *
     * @param wx Abscisse du coin supérieur gauche où afficher le message.
     * @param wy Ordonnée du coin supérieur gauche où afficher le message.
     * @param text Texte du message à afficher.
     */
    void print(std::size_t wx, std::size_t wy, std::string text...) const;

private:
    unsigned int _width;
    unsigned int _height;
    WINDOW* _mesgwin;
    WINDOW* _inside;
};

#endif //__TENTEN_MESSAGE_HPP_INCLUDED__

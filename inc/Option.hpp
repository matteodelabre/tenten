/**
 * @file
 * @brief Définit la classe @c Option.
 */

#ifndef __TENTEN_OPTION_INCLUDED__
#define __TENTEN_OPTION_INCLUDED__

#include <string>

/**
 * @brief Représente une des options passées au programme.
 */
class Option
{
public:
    /* ÉNUMÉRATIONS */

    /**
     * @brief Types d’options possibles.
     */
    enum class Type
    {
        FLAG,
        INT,
        STRING
    };

    /* CONSTRUCTEURS */

    /**
     * @brief Construit une option vide dont le contenu n’est pas garanti.
     */
    Option();

    /**
     * @brief Construit une option de nom court, nom long, description et
     * type donnés.
     *
     * @param shortname Nom court de l’option.
     * @param longname Nom long de l’option.
     * @param description Description de l’option.
     * @param type Type de données de l’option.
     */
    Option(
        int shortname, std::string longname,
        std::string description, Type type
    );

    /* ACCESSEURS */

    /**
     * @brief Vérifie si l’option a été passée au programme, après analyse
     * des arguments.
     *
     * @return Vrai si et seulement si l’option a été passée au programme.
     */
    bool isPassed() const;

    /**
     * @brief Modifie l’état passé ou non passé de l’option.
     * @param is_passed Si l’état est passé ou non.
     */
    void setPassed(bool is_passed);

    /**
     * @brief Récupère la valeur entière de l’option, seulement si l’option
     * est de type entier.
     * @param value Paramètre dans lequel écrire la valeur.
     */
    void getValue(int& value) const;

    /**
     * @brief Récupère la valeur chaîne de caractères de l’option, seulement
     * si l’option est de type chaîne de caractères.
     * @param value Paramètre dans lequel écrire la valeur.
     */
    void getValue(std::string& value) const;

    /**
     * @brief Modifie la valeur de l’option, en convertissant la chaîne vers
     * le bon type de données.
     * @param value Valeur à affecter à l’option.
     */
    void setValue(std::string value);

    /**
     * @brief Récupère le nom court de l’option.
     */
    int getShortname() const;

    /**
     * @brief Récupère le nom long de l’option.
     */
    std::string getLongname() const;

    /**
     * @brief Récupère la description de l’option.
     */
    std::string getDescription() const;

    /**
     * @brief Récupère le type de l’option.
     */
    Type getType() const;

private:
    int _shortname;
    std::string _longname;
    std::string _description;
    Type _type;

    bool _is_passed;
    int _value_int;
    std::string _value_string;
};

#endif // __TENTEN_OPTION_INCLUDED__

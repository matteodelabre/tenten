/**
 * @file
 * @brief Définit la classe @c OptionList.
 */

#ifndef __TENTEN_OPTIONLIST_INCLUDED__
#define __TENTEN_OPTIONLIST_INCLUDED__

#include <unordered_map>
#include <iostream>

#include "Option.hpp"

/**
 * @brief Gère les options passées au programme et les messages d’aide
 * et de version liés au programme.
 */
class OptionList
{
public:
    /* CONSTRUCTEURS */

    /**
     * @brief Construit une liste d’options avec les paramètres donnés.
     *
     * @param name Nom du programme dont on analyse les options.
     * @param version Numéro de version du programme.
     * @param authors Liste des auteurs du programme.
     */
    OptionList(
        std::string name, std::string version,
        std::string authors
    );

    /* ACCESSEURS */

    /**
     * @brief Ajoute une option au programme.
     *
     * @param option Option à ajouter.
     */
    void addOption(Option option);

    /**
     * @brief Récupère une option par son nom court.
     *
     * @param shortname Nom court de l’option.
     * @return Option correspondante.
     */
    const Option& getOption(int shortname) const;

    /* ACTIONS */

    /**
     * @brief Affiche le message d’aide sur le flux donné.
     *
     * @param out Flux dans lequel écrire le message d’aide.
     */
    void help(std::ostream& out = std::cout);

    /**
     * @brief Affiche le numéro de version du programme.
     *
     * @param out Flux dans lequel écrire le numéro de version.
     */
    void version(std::ostream& out = std::cout);

    /**
     * @brief Interprète les arguments en provenance de la fonction principale
     * comme des options et remplit les Options ajoutées en conséquence.
     *
     * @param argc Nombre d’arguments passés au programme.
     * @param argv Contenu des arguments.
     * @param Vrai seulement s’il n’y a aucune erreur dans les arguments passés.
     */
    bool parse(unsigned int argc, char** argv);

private:
    std::string _name;
    std::string _version;
    std::string _authors;

    std::unordered_map<int, Option> _map;
};

#endif // __TENTEN_ARGUMENTS_INCLUDED__

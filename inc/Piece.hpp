/**
 * @file
 * @brief Définit la classe @c Piece
 */

#ifndef __TENTEN_PIECE_HPP_INCLUDED__
#define __TENTEN_PIECE_HPP_INCLUDED__

#include <cstdlib>

#include "Grid.hpp"

/**
 * @brief Représente une pièce du jeu
 *
 * Chaque pièce est associée à une grille définissant sa forme. Cette grille
 * a une cellule-pivot qui est la cellule d'origine, par rapport à laquelle
 * la pièce est placée. Toute pièce peut être dans l'état "placée" ou
 * "non-placée"
 */
class Piece
{
public:
    /* CONSTRUCTEURS */

    /**
     * @brief Construit une pièce par défaut.
     */
    Piece();

    /**
     * @brief Construit une nouvelle pièce de forme @p shape
     * et de pivot (@p pivot_x, @p pivot_y)
     *
     * @param shape Grille définissant la forme de la pièce
     * @param pivot_x Abscisse du pivot de la pièce
     * @param pivot_y Ordonnée du pivot de la pièce
     */
    Piece(Grid _shape, std::size_t _pivot_x = 0, std::size_t _pivot_y = 0);

    /* ACCESSEURS */

    /**
     * @brief Récupère la grille associé à la pièce
     * @return La forme de la pièce
     */
    const Grid& getShape() const;

    /**
     * @brief Récupère l'abscisse du pivot de la pièce
     * @return L'abscisse du pivot
     */
    std::size_t getPivotX() const;

    /**
     * @brief Récupère l'ordonnée du pivot de la pièce
     * @return L'ordonnée du pivot
     */
    std::size_t getPivotY() const;

    /**
     * @brief La pièce a-t-elle déjà été placée ?
     */
    bool isPlaced() const;

    /**
     * @brief Modifie si la pièce a été placée ou non
     * @param is_placed Si la pièce a été placée ou non
     */
    void setPlaced(bool is_placed);

    /* FICHIERS */

    /**
     * @brief Lit les attributs d’une pièce depuis le flux @p out.
     *
     * @param in Flux depuis lequel lire.
     */
    void read(std::istream& in);

    /**
     * @brief Écrit les attributs de la pièce dans le flux @p out.
     *
     * @param out Flux dans lequel écrire le contenu.
     */
    void write(std::ostream& sortie) const;

private:
    Grid _shape;
    std::size_t _pivot_x;
    std::size_t _pivot_y;
    bool _is_placed;
};

#endif // __TENTEN_PIECE_HPP_INCLUDED__

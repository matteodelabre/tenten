/**
 * @file
 * @brief Définit la classe @c Scores.
 */

#ifndef __TENTEN_SCORES_HPP_INCLUDED__
#define __TENTEN_SCORES_HPP_INCLUDED__

#include <vector>
#include <utility>
#include <algorithm>
#include <string>

typedef std::pair<std::string, unsigned int> ScorePair;

/**
 * @brief Gère les meilleurs scores, l’écriture et la lecture du fichier des
 * scores et l’ajout d’un meilleur score.
 */
class Scores
{
public:
    /**
     * @brief Initialise une instance pour gérer les scores.
     */
    Scores();

    /**
     * @brief Met à jour le fichier de sauvegarde des scores.
     */
    void write() const;

    /**
     * @brief Lit le tableau des scores à partir du fichier de sauvegarde.
     */
    void read();

    /**
     * @brief Ajoute un score dans la liste des meilleurs scores.
     * @param pair Paire de score à ajouter.
     */
    void addScore(ScorePair pair);

    /**
     * @brief Renvoie le classement sous forme de liste de paires nom/score.
     * @return Liste des paires.
     */
    const std::vector<ScorePair>& getPlaces() const;

private:
    std::vector<ScorePair> _all_scores;
};

/**
 * @brief Compare deux paires par ordre de score.
 * @param gauche Première paire.
 * @param droite Seconde paire.
 */
bool sortScore(const ScorePair gauche, const ScorePair droite);

#endif // __TENTEN_SCORES_HPP_INCLUDED__

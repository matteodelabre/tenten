/**
 * @file
 * @brief Définit la classe @c TenTen
 */

#ifndef __TENTEN_TEN_TEN_HPP_INCLUDED__
#define __TENTEN_TEN_TEN_HPP_INCLUDED__

#include "Game.hpp"
#include "OptionList.hpp"

/**
 * @brief Gère le programme ainsi que les instances du jeu
 *
 * Crée une seul instance du jeu à la fois, les unes après les autres.
 * Conserve le meilleur score des différentes instances du jeu.
 */
class TenTen
{
public:
    /* CONSTRUCTEURS */

    /**
     * @brief Construit une instance de TenTen
     */
    TenTen();

    /* ACTIONS */

    /**
     * @brief Lance la boucle principale permettant au joueur de jouer autant
     * de parties qu’il le souhaite en conservant son meilleur score.
     *
     * @param argc Nombre d’arguments passés.
     * @param argv Valeur des arguments passés.
     * @return Code de sortie du programme.
     */
    int start(int argc, char** argvs);

private:
    OptionList _options;
    Scores _scores;
};

#endif // __TENTEN_TEN_TEN_HPP_INCLUDED__

#ifndef __TENTEN_TOOLS_HPP_INCLUDED__
#define __TENTEN_TOOLS_HPP_INCLUDED__

#include <string>
#include <iostream>

/**
 * Supprime les espaces au début et à la fin de la chaîne @p str.
 *
 * @param str La chaîne à nettoyer.
 * @return Une copie de la chaîne sans espaces initiaux et finaux.
 */
std::string trimWhitespace(std::string str);

/**
 * Ignore tous les caractères dans le flux @p in jusqu’au prochain
 * qui n’est pas un des caractères donnés, ou la fin du flux. Avance
 * le curseur jusqu’au premier caractère différent exclu.
 *
 * @param in Flux dans lequel ignorer les caractères.
 * @param chars Caractères à ignorer, par défaut tous les blancs.
 */
void ignoreChars(std::istream& in, std::string chars = " \n\t");

/**
 * Ignore tous les caractères dans le flux @p in jusqu’à arriver à
 * l’un des caractères donnés, ou la fin du flux. Avance le curseur
 * jusqu’au premier caractère correspondant inclus.
 *
 * @param in Flux dans lequel ignorer les caractères.
 * @param chars Caractères à atteindre.
 */
void ignoreUntil(std::istream& in, std::string chars);

/**
 * Lit un champ au format "Champ : " à la position actuelle du curseur,
 * et renvoie ce champ. Avance le curseur jusqu’à après ':' ou la fin
 * de la ligne s’il n’y pas de ':' dedans.
 *
 * @param in Flux dans lequel lire un champ.
 * @return Le champ lu, ou la chaîne vide si aucun champ sur la ligne.
 */
std::string readField(std::istream& in);

#endif // __TENTEN_TOOLS_HPP_INCLUDED__

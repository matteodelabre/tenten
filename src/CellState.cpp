#include "CellState.hpp"
#include "Color.hpp"

#include <iostream>
#include <ncurses.h>

int stateToColor(CellState state)
{
    switch (state)
    {
    case CellState::BLACK:
        return ' ' | COLOR_PAIR(ColorPairs::BG_WHITE);

    case CellState::YELLOW:
        return ' ' | COLOR_PAIR(ColorPairs::BG_YELLOW);

    case CellState::BLUE:
        return ' ' | COLOR_PAIR(ColorPairs::BG_BLUE);

    case CellState::GREEN:
        return ' ' | COLOR_PAIR(ColorPairs::BG_GREEN);

    case CellState::RED:
        return ' ' | COLOR_PAIR(ColorPairs::BG_RED);

    case CellState::EMPTY:
        return ' ';

    default:
        throw std::invalid_argument(
            "L'état de la cellule n'est pas reconnu."
        );
    }
}

char stateToChar(CellState state)
{
    switch(state)
    {
    case CellState::BLACK:
        return 'K';

    case CellState::YELLOW:
        return 'Y';

    case CellState::BLUE:
        return 'B';

    case CellState::GREEN:
        return 'G';

    case CellState::RED:
        return 'R';

    case CellState::EMPTY:
        return ' ';

    default:
        throw std::invalid_argument(
            "L'état de la cellule n'est pas reconnu."
        );
    }
}

CellState charToState(char character)
{
    switch (character)
    {
    case ' ':
        return CellState::EMPTY;

    case 'R':
        return CellState::RED;

    case 'G':
        return CellState::GREEN;

    case 'B':
        return CellState::BLUE;

    case 'Y':
        return CellState::YELLOW;

    case 'K':
        return CellState::BLACK;

    default:
        throw std::invalid_argument
        (
            std::string("Le caractère '") + character + "' n'est pas reconnu" +
            " comme étant une couleur."
        );
    }
}

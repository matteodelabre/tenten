#include "Color.hpp"
#include <ncurses.h>

void initColorPairs()
{
    init_pair(ColorPairs::BG_RED, COLOR_RED, COLOR_RED);
    init_pair(ColorPairs::BG_GREEN, COLOR_GREEN, COLOR_GREEN);
    init_pair(ColorPairs::BG_BLUE, COLOR_BLUE, COLOR_BLUE);
    init_pair(ColorPairs::BG_YELLOW, COLOR_YELLOW, COLOR_YELLOW);
    init_pair(ColorPairs::BG_WHITE, COLOR_WHITE, COLOR_WHITE);
    init_pair(ColorPairs::SELECTED, COLOR_RED, COLOR_BLACK);
}

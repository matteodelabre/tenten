#include "Game.hpp"
#include "GameState.hpp"
#include "Message.hpp"
#include "tools.hpp"

#include <cstdlib>
#include <iostream>
#include <cctype>

// Constante contenant les formes de pièces par défaut du jeu
const std::vector<Piece> default_pieces = {
    Piece(Grid(1, "K"), 0, 0),
    Piece(Grid(1, "B"
                  "B"), 0, 0),
    Piece(Grid(1, "G"
                  "G"
                  "G"), 0, 1),
    Piece(Grid(1, "Y"
                  "Y"
                  "Y"
                  "Y"), 0, 1),
    Piece(Grid(1, "R"
                  "R"
                  "R"
                  "R"
                  "R"), 0, 2),
    Piece(Grid(2, "BB"), 0, 0),
    Piece(Grid(3, "GGG"), 1, 0),
    Piece(Grid(4, "YYYY"), 1, 0),
    Piece(Grid(2, "GG"
                  " G"), 0, 0),
    Piece(Grid(2, "Y "
                  "YY"), 0, 0),
    Piece(Grid(2, "R "
                  "RR"
                  " R"), 0, 1),
    Piece(Grid(2, "RR"
                  "RR"), 0, 0),
    Piece(Grid(3, "BBB"
                  "  B"), 1, 0)
};

Game::Game(Scores* scores)
: _current_piece(0), _current_x(0), _current_y(0), _piece_chosen(false),
  _available_pieces(default_pieces), _scores(scores),
  _main_gridview(Grid()), _pieces_gridviews(3, Grid()),
  _shortcuts(19, 9), _score(19, 7)
{
    _states.emplace(
        Grid(),
        choosePieces(3),
        0
    );
}

std::vector<Piece> Game::choosePieces(std::size_t quantity) const
{
    std::vector<Piece> result;
    std::size_t max = _available_pieces.size();

    if (max == 0 && quantity > 0)
    {
        throw std::logic_error(
            std::string("Impossible de choisir ") + std::to_string(quantity) +
            " pièces parmi zéro formes disponibles."
        );
    }

    for (std::size_t i = 0; i < quantity; i++)
    {
        result.push_back(_available_pieces.at(rand() % max));
    }

    return result;
}

bool Game::next()
{
    // Récupération de l'état actuel (on suppose qu'il existe déjà au moins
    // un état) et copie de ses composants pour les modifier
    const GameState& current = _states.top();

    Grid main_grid(current.getMainGrid());
    std::vector<Piece> remaining_pieces(current.getRemainingPieces());
    unsigned int score = current.getScore();

    // Vérification que la pièce demandée ne soit pas déjà placée
    Piece& piece = remaining_pieces.at(_current_piece);

    if (piece.isPlaced())
    {
        return false;
    }
    else
    {
        piece.setPlaced(true);
    }

    // Ajout de la pièce dans la grille principale, si le coup est légal
    if (!main_grid.isAllowed(piece, _current_x, _current_y))
    {
        return false;
    }
    else
    {
        main_grid.placePiece(piece, _current_x, _current_y);
    }

    // Vidage des lignes pleines et incrémentation du score
    score += main_grid.clean();

    // Vérifie s’il reste encore des pièces à placer
    bool has_remaining = false;
    std::size_t i = 0;

    while (i < remaining_pieces.size() && !has_remaining)
    {
        if (!remaining_pieces.at(i).isPlaced())
        {
            has_remaining = true;
        }

        i++;
    }

    // S’il ne reste plus de pièces à placer, renouvellement
    if (!has_remaining)
    {
        remaining_pieces = choosePieces(remaining_pieces.size());
    }

    // Création et empilage du prochain état
    _states.emplace(main_grid, remaining_pieces, score);
    return true;
}

void Game::previous()
{
    // Retour à l'état précédent, s'il en reste un
    if (_states.size() >= 2)
    {
        _states.pop();
    }
}

void Game::chooseNextPiece()
{
    const std::vector<Piece>& pieces = _states.top().getRemainingPieces();

    do
    {
        if (_current_piece >= pieces.size() - 1)
        {
            _current_piece = 0;
        }
        else
        {
            _current_piece++;
        }
    }
    while (pieces.at(_current_piece).isPlaced());
}

void Game::choosePreviousPiece()
{
    const std::vector<Piece>& pieces = _states.top().getRemainingPieces();

    do
    {
        if (_current_piece == 0)
        {
            _current_piece = pieces.size() - 1;
        }
        else
        {
            _current_piece--;
        }
    }
    while (pieces.at(_current_piece).isPlaced());
}

void Game::checkCurrentPiece()
{
    const std::vector<Piece>& pieces = _states.top().getRemainingPieces();

    if (pieces.at(_current_piece).isPlaced())
    {
        chooseNextPiece();
    }
}

Game::Exit Game::play()
{
    // Nettoyage initial de l’écran
    erase();

    while (true)
    {
        const Grid& grid = _states.top().getMainGrid();
        const std::vector<Piece>& pieces = _states.top().getRemainingPieces();

        // S’assure qu’on n’a pas sélectionné une pièce déjà placée
        checkCurrentPiece();

        const Piece& piece = pieces.at(_current_piece);
        const Grid& shape = piece.getShape();

        // S’il n’y a plus de possibilités, on sort en proposant éventuellement
        // d’enregistrer le score
        if (!grid.isPossible(pieces))
        {
            def_prog_mode();
            endwin();

            std::string name;
            std::cout << "Saisissez votre nom : ";
            std::cin >> name;

            _scores->addScore(std::make_pair(name, _states.top().getScore()));
            _scores->write();

            reset_prog_mode();
            return Exit::END;
        }

        // Affichage de l’interface du jeu
        print();

        // Interprétation du clavier
        switch (getch())
        {
        case '\n':
            if (_piece_chosen)
            {
                // Validation du placement d’une pièce
                if (next())
                {
                    _piece_chosen = false;
                    _current_x = 0;
                    _current_y = 0;
                }
            }
            else
            {
                // Validation du choix de la pièce à placer
                _piece_chosen = true;
                _current_x = 5;
                _current_y = 5;
            }
            break;

        case  3: // Ctrl + C
        case 17: // Ctrl + Q
            // Quitter le jeu
            return Exit::QUIT;

        case 18: // Ctrl + R
            // Quitter la partie et en commencer une autre
            return Exit::END;

        case 19: { // Ctrl + S
            // Sauvegarder la partie actuelle
            def_prog_mode();
            endwin();

            std::string name;
            std::cout << "Choisissez un nom de sauvegarde : ";
            std::cin >> name;

            reset_prog_mode();
            write(name);
            break;
        }

        case 26: // Ctrl + Z
            // Annulation de la dernière action
            previous();
            break;

        case 27: // Échap
            // Annulation du choix de la pièce
            _piece_chosen = false;
            break;

        case KEY_UP:
            if (_piece_chosen)
            {
                if (_current_y - piece.getPivotY() > 0)
                {
                    _current_y--;
                }
            }
            else
            {
                choosePreviousPiece();
            }
            break;

        case KEY_LEFT:
            if (_piece_chosen)
            {
                if (_current_x - piece.getPivotX() > 0)
                {
                    _current_x--;
                }
            }
            break;

        case KEY_DOWN:
            if (_piece_chosen)
            {
                if (
                    _current_y + shape.getHeight() - piece.getPivotY() - 1
                     < grid.getHeight() - 1
                )
                {
                    _current_y++;
                }
            }
            else
            {
                chooseNextPiece();
            }
            break;

        case KEY_RIGHT:
            if (_piece_chosen)
            {
                if (
                    _current_x + shape.getWidth() - piece.getPivotX() - 1
                    < grid.getWidth() - 1
                )
                {
                    _current_x++;
                }
            }
            break;

        case KEY_RESIZE:
            // Redimensionnement : on redessine tout l’écran
            erase();
            refresh();
            break;
        }
    }
}

void Game::write(std::string name) const
{
    std::ofstream out("../save/" + name + ".tenten", std::ios::out);

    if (!out.is_open())
    {
        throw std::runtime_error(
            std::string("Impossible d’écrire le fichier de sauvegarde du ") +
            "jeu : " + strerror(errno)
        );
    }

    // Signature du fichier : #tenten_game
    out << "#tenten_game" << std::endl;

    // Première section : liste des pièces plaçables
    out << std::endl << "#available_pieces" << std::endl << std::endl;

    for (size_t i = 0; i < _available_pieces.size(); i++)
    {
        _available_pieces.at(i).write(out);
        out << std::endl << std::endl;
    }

    // Deuxième section : liste des états du plus ancien au plus récent
    out << std::endl << "#states" << std::endl << std::endl;

    // On dépile d’abord les états dans un vecteur afin d’avoir la pile
    // dans l’autre sens
    std::stack<GameState> states = _states;
    std::vector<GameState> reverse_states(states.size());
    std::size_t i = reverse_states.size() - 1;

    while (!states.empty())
    {
        reverse_states.at(i) = states.top();
        states.pop();
        i--;
    }

    for (i = 0; i < reverse_states.size(); i++)
    {
        reverse_states.at(i).write(out);
        out << std::endl << std::endl;
    }
}

void Game::read(std::string name)
{
    std::ifstream in("../save/" + name + ".tenten", std::ios::in);

    if (!in.is_open())
    {
        throw std::runtime_error(
            std::string("Impossible de lire le fichier de sauvegarde du ") +
            "jeu : " + strerror(errno)
        );
    }

    // Vidage de la pile et des pièces disponibles
    _available_pieces.clear();

    while (!_states.empty())
    {
        _states.pop();
    }

    // Vérification de la ligne d’en-tête
    std::string line;
    std::getline(in, line);

    if (line != "#tenten_game")
    {
        throw std::runtime_error(
            "Le fichier n’est pas au format de partie TenTen."
        );
    }

    // Va à la première section
    ignoreUntil(in, "#");

    while (std::getline(in, line))
    {
        ignoreChars(in);

        if (line == "states")
        {
            // Lecture de la liste des états du jeu
            while (in.peek() != '#' && in)
            {
                _states.push(GameState());
                _states.top().read(in);
                ignoreChars(in);
            }

            in.get();
        }
        else if (line == "available_pieces")
        {
            // Lecture de la liste des pièces disponibles
            while (in.peek() != '#' && in)
            {
                _available_pieces.push_back(Piece());
                _available_pieces.at(_available_pieces.size() - 1).read(in);
                ignoreChars(in);
            }

            in.get();
        }
        else
        {
            throw std::runtime_error(
                std::string("La section #") + line + " n’est pas reconnue."
            );
        }
    }

    // Création d’un état initial si nécessaire
    if (_states.empty())
    {
        _states.emplace(
            Grid(),
            choosePieces(3),
            0
        );
    }
}

void Game::print()
{
    const std::vector<Piece>& pieces = _states.top().getRemainingPieces();

    int term_width, term_height;
    getmaxyx(stdscr, term_height, term_width);

    refresh();

    // Affichage de la grille principale actuelle, en rajoutant si nécessaire
    // la pièce actuelle par dessus dans une copie (aperçu du placement)
    Grid main_grid = _states.top().getMainGrid();

    if (_piece_chosen)
    {
        main_grid.placePiece(pieces.at(_current_piece), _current_x, _current_y);
    }

    _main_gridview.setGrid(main_grid);
    _main_gridview.print(term_width / 2, term_height / 2 - 5);

    // Effacement des précédentes pièces à placer
    for (std::size_t i = 0; i < _pieces_gridviews.size(); i++)
    {
        _pieces_gridviews.at(i).erase();
    }

    // Affichage des pièces à placer à gauche
    std::size_t next_y = term_height / 2 - 5;

    for (std::size_t i = 0; i < pieces.size(); i++)
    {
        const Grid& shape = pieces.at(i).getShape();
        GridView::State state = GridView::State::NORMAL;

        if (i == _current_piece)
        {
            state = GridView::State::SELECTED;
        }
        else if (pieces.at(i).isPlaced())
        {
            state = GridView::State::DISABLED;
        }

        _pieces_gridviews.at(i).setGrid(shape);
        _pieces_gridviews.at(i).print(
            term_width / 2 - 3 - shape.getWidth(),
            next_y, state
        );

        next_y += shape.getHeight() + 2;
    }

    // Affichage des raccourcis
    _shortcuts.print(
        2, term_height / 2 - 5,
        "Touches          \n"
        "-------          \n"
        "Annuler      Ctrl+Z"
        "Sauvegarder  Ctrl+S"
        "Recommencer  Ctrl+R"
        "Quitter      Ctrl+Q"
        "Deplacer    Fleches"
        "Valider      Entree"
        "Annuler       Echap"
    );

    // Liste des meilleurs scores
    const std::vector<ScorePair>& list = _scores->getPlaces();
    std::string scores_text = std::string("Score : ")
        + std::to_string(_states.top().getScore()) + "\n-----\n";

    for (std::size_t i = 0; i < list.size(); i++)
    {
        scores_text += std::to_string(i + 1) + ". " +
            list.at(i).first.substr(0, 8) + " : " +
            std::to_string(list.at(i).second) + "\n";
    }

    _score.print(
        term_width - 23, term_height / 2 - 5,
        scores_text
    );
}

#include "GameState.hpp"
#include "tools.hpp"

GameState::GameState()
: _main_grid(), _remaining_pieces(), _score(0)
{}

GameState::GameState(
    Grid _main_grid,
    std::vector<Piece> _remaining_pieces,
    unsigned int _score
)
:_main_grid(_main_grid), _remaining_pieces(_remaining_pieces), _score(_score)
{}

const Grid& GameState::getMainGrid() const
{
    return _main_grid;
}

const std::vector<Piece>& GameState::getRemainingPieces() const
{
    return _remaining_pieces;
}

const unsigned int& GameState::getScore() const
{
    return _score;
}

void GameState::read(std::istream& in)
{
    std::string field;

    // Vidage des pièces à placer
    _remaining_pieces.clear();

    do
    {
        field = readField(in);
        ignoreChars(in, " \t");

        if (field.size())
        {
            if (field == "Score")
            {
                in >> _score;
            }

            if (field == "Grid")
            {
                _main_grid.read(in);
            }

            if (field == "Remaining")
            {
                ignoreUntil(in, "[");
                ignoreChars(in);

                while (in.peek() != ']')
                {
                    _remaining_pieces.push_back(Piece());
                    _remaining_pieces.at(_remaining_pieces.size() - 1).read(in);
                    ignoreChars(in);
                }

                in.get();
            }

            ignoreChars(in, " \t");

            if (in.get() != '\n')
            {
                throw std::runtime_error(
                    std::string("Caractères inattendus à la fin du champ '")
                    + field + "'."
                );
            }
        }
    }
    while (field.size());
}

void GameState::write(std::ostream& out) const
{
    out << "Score : " << _score << std::endl;
    _main_grid.write(out);
    out << std::endl;

    out << "Remaining :" << std::endl << "[" << std::endl;

    for (std::size_t i = 0; i < _remaining_pieces.size(); i++)
    {
        _remaining_pieces.at(i).write(out);
        out << std::endl << std::endl;
    }

    out << std::endl << "]";
}

#include "Grid.hpp"
#include "Piece.hpp"
#include "CellState.hpp"
#include "Color.hpp"
#include "tools.hpp"

#include <vector>
#include <stdexcept>
#include <algorithm>

// Constante permettant de convertir le nombre de lignes nettoyées
// en nombre de points accordés au joueur
const unsigned int score_table[] = {0, 10, 25, 45, 60, 75, 90};

Grid::Grid()
: _width(10), _height(10),
  _content(10, std::vector<CellState>(10, CellState::EMPTY))
{}

Grid::Grid(std::size_t width, std::string content)
: _width(width), _height(width == 0 ? 0 : content.size() / width),
  _content(_width, std::vector<CellState>(_height, CellState::EMPTY))
{
    for (std::size_t i = 0; i < _width * _height; i++)
    {
        std::size_t x = i % _width;
        std::size_t y = i / _width;

        // Modification de la case actuelle pour correspondre au caractère
        // dans la chaîne d’entrée
        _at(x, y) = charToState(content[i]);
    }
}

std::size_t Grid::getWidth() const
{
    return _width;
}

std::size_t Grid::getHeight() const
{
    return _height;
}

const CellState& Grid::at(std::size_t x, std::size_t y) const
{
    return _content.at(x).at(y);
}

void Grid::placePiece(const Piece& piece, std::size_t x, std::size_t y)
{
    for (std::size_t i = 0; i < piece.getShape().getWidth(); i++)
    {
        for (std::size_t j = 0 ; j < piece.getShape().getHeight(); j++)
        {
            const CellState& cell = piece.getShape().at(i, j);

            // Écrasement du contenu de la case de la grille par le contenu
            // de la case de la pièce
            if (cell != CellState::EMPTY)
            {
                _at(x - piece.getPivotX() + i, y - piece.getPivotY() + j)
                    = cell;
            }
        }
    }
}

unsigned int Grid::clean()
{
    // Listage des lignes et colonnes pleines
    std::vector<int> full_columns;
    std::vector<int> full_lines;

    for (size_t x = 0; x < getWidth(); x++)
    {
        if (isFull(x, true))
        {
            full_columns.push_back(x);
        }
    }

    for (size_t y = 0; y < getHeight(); y++)
    {
        if (isFull(y, false))
        {
            full_lines.push_back(y);
        }
    }

    // Nettoyage des lignes et colonnes pleines
    for (size_t i = 0; i < full_columns.size(); i++)
    {
        cleanFull(full_columns.at(i), true);
    }

    for (size_t i = 0; i < full_lines.size(); i++)
    {
        cleanFull(full_lines.at(i), false);
    }

    // Comptabilisation du score
    unsigned int count_lines = full_lines.size();
    unsigned int count_columns = full_columns.size();
    unsigned int score =
        score_table[count_lines > 6 ? 6 : count_lines] +
        score_table[count_columns > 6 ? 6 : count_columns];

    if (count_lines > 0 && count_columns > 0)
    {
        score *= 2;
    }

    return score;
}

bool Grid::isFull(std::size_t index, bool is_column) const
{
    std::size_t length = is_column ? getHeight() : getWidth();
    std::size_t i = 0;
    bool full = true;

    while (i < length && full)
    {
        if (is_column && at(index, i) == CellState::EMPTY)
        {
            full = false;
        }

        if (!is_column && at(i, index) == CellState::EMPTY)
        {
            full = false;
        }

        i++;
    }

    return full;
}

void Grid::cleanFull(std::size_t index, bool is_column)
{
    std::size_t length = is_column ? getHeight() : getWidth();

    for (std::size_t i = 0; i < length; i++)
    {
        if (is_column)
        {
            _at(index, i) = CellState::EMPTY;
        }
        else
        {
            _at(i, index) = CellState::EMPTY;
        }
    }
}

CellState& Grid::_at(std::size_t x, std::size_t y)
{
    return _content.at(x).at(y);
}

bool Grid::isAllowed(const Piece& piece, std::size_t x, std::size_t y) const
{
    bool is_allowed = true;
    const Grid& shape = piece.getShape();

    std::size_t piece_x = 0;
    std::size_t piece_y = 0;

    while (is_allowed && piece_x < shape._width)
    {
        while (is_allowed && piece_y < shape._height)
        {
            std::size_t grid_x = x + piece_x - piece.getPivotX();
            std::size_t grid_y = y + piece_y - piece.getPivotY();

            // Vérification de la validité du placement d’une case. Il est
            // invalide si en dehors de la grille d’arrivée ou si la case en
            // écrase une pleine
            if (
                (
                    grid_x >= _width || grid_y >= _height
                )
                ||
                (
                    shape.at(piece_x, piece_y) != CellState::EMPTY &&
                    at(grid_x, grid_y) != CellState::EMPTY
                )
            )
            {
                is_allowed = false;
            }

            piece_y++;
        }

        piece_x++;
        piece_y = 0;
    }

    return is_allowed;
}

bool Grid::isPossible(const std::vector<Piece>& pieces) const
{
    bool is_possible = false;
    std::size_t i = 0;

    while (!is_possible && i < pieces.size())
    {
        std::size_t to_x = 0;
        std::size_t to_y = 0;

        if (!pieces.at(i).isPlaced())
        {
            while (!is_possible && to_x < _width)
            {
                while (!is_possible && to_y < _height)
                {
                    is_possible = isAllowed(pieces.at(i), to_x, to_y);
                    to_y++;
                }

                to_x++;
                to_y = 0;
            }
        }

        i++;
    }

    return is_possible;
}

void Grid::read(std::istream& in)
{
    // Lecture de la largeur
    in >> _width;

    // Avance jusqu’à la parenthèse ouvrante du contenu
    ignoreUntil(in, "(");

    // Lecture du contenu de la grille
    _content = std::vector<std::vector<CellState>>(_width);

    int read = in.get();
    std::size_t x = 0;

    while (read != ')' && in)
    {
        if (read != '\n' && read != ',')
        {
            _content.at(x).push_back(charToState(read));
            x = (x + 1) % _width;
        }
        else if (x > 0)
        {
            // Si la ligne n’est pas finie entièrement, on bouche
            // avec des cases vides
            for (; x < _width; x++)
            {
                _content.at(x).push_back(CellState::EMPTY);
            }

            x = 0;
        }

        read = in.get();
    }

    _height = _content.at(0).size();
}

void Grid::write(std::ostream& out) const
{
    out << "Grid : " << _width << std::endl << "(" << std::endl;

    for (std::size_t y = 0; y < getHeight(); y++)
    {
        for (std::size_t x = 0; x < getWidth(); x++)
        {
            out << stateToChar(_content[x][y]);
        }

        out << "," << std::endl;
    }

    out << ")";
}

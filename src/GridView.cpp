#include "GridView.hpp"
#include "Color.hpp"
#include "CellState.hpp"

GridView::GridView(Grid grid)
: _grid(grid), _gridwin(newwin(grid.getHeight() + 2, grid.getWidth() + 2, 0, 0))
{}

GridView::GridView(const GridView& src)
: _grid(src._grid),
  _gridwin(newwin(_grid.getHeight() + 2, _grid.getWidth() + 2, 0, 0))
{}

GridView& GridView::operator=(const GridView& src)
{
    if (this != &src)
    {
        setGrid(src._grid);
    }

    return *this;
}

GridView::~GridView()
{
    delwin(_gridwin);
}

void GridView::setGrid(Grid grid)
{
    if (
        grid.getWidth() != _grid.getWidth() ||
        grid.getHeight() != _grid.getHeight()
    )
    {
        wresize(_gridwin, grid.getHeight() + 2, grid.getWidth() + 2);
    }

    _grid = grid;
}

void GridView::erase() const
{
    werase(_gridwin);
    wrefresh(_gridwin);
}

void GridView::print(std::size_t wx, std::size_t wy, State state) const
{
    mvwin(_gridwin, wy, wx);
    wattrset(_gridwin, A_NORMAL);

    // Affichage du contenu de la grille, sauf en mode désactivé
    if (state != State::DISABLED)
    {
        for (std::size_t x = 0; x < _grid.getWidth(); x++)
        {
            for (std::size_t y = 0; y < _grid.getHeight(); y++)
            {
                mvwaddch(
                    _gridwin,
                    y + 1, x + 1,
                    stateToColor(_grid.at(x, y))
                );
            }
        }
    }

    // Coloration de la bordure en mode sélectionné
    if (state == State::SELECTED)
    {
        wattrset(_gridwin, COLOR_PAIR(ColorPairs::SELECTED));
    }
    else
    {
        wattrset(_gridwin, A_NORMAL);
    }

    wborder(_gridwin, 0, 0, 0, 0, 0, 0, 0, 0);
    wrefresh(_gridwin);
}

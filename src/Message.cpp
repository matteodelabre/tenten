#include "Message.hpp"
#include <cstdarg>

Message::Message(unsigned int width, unsigned int height)
: _width(width), _height(height),
  _mesgwin(newwin(_height + 2, _width + 2, 0, 0)),
  _inside(derwin(_mesgwin, _height, _width, 1, 1))
{}

Message::Message(const Message& src)
: _width(src._width), _height(src._height),
  _mesgwin(newwin(_height + 2, _width + 2, 0, 0)),
  _inside(derwin(_mesgwin, _height, _width, 1, 1))
{}

Message& Message::operator=(const Message& src)
{
    if (this != &src)
    {
        _width = src._width;
        _height = src._height;
        wresize(_mesgwin, _height + 2, _width + 2);
    }

    return *this;
}

Message::~Message()
{
    delwin(_mesgwin);
}

void Message::erase() const
{
    werase(_mesgwin);
    wrefresh(_mesgwin);
}

void Message::print(std::size_t wx, std::size_t wy, std::string text...) const
{
    va_list args;
    va_start(args, text);

    mvwin(_mesgwin, wy, wx);
    wmove(_inside, 0, 0);
    vw_printw(_inside, text.c_str(), args);
    wborder(_mesgwin, 0, 0, 0, 0, 0, 0, 0, 0);
    wrefresh(_mesgwin);
}

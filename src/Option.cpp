#include "Option.hpp"

#include <stdexcept>
#include <string>

Option::Option()
{}

Option::Option(
    int shortname, std::string longname,
    std::string description, Type type
)
: _shortname(shortname), _longname(longname), _description(description),
  _type(type), _is_passed(false)
{}

bool Option::isPassed() const
{
    return _is_passed;
}

void Option::getValue(int& value) const
{
    if (_type == Type::INT)
    {
        value = _value_int;
    }
    else
    {
        throw std::logic_error(
            std::string("L’option ") + _longname + " n’est pas de type entier. "
            + "Il n’est pas possible de récupérer la valeur comme entier."
        );
    }
}

void Option::getValue(std::string& value) const
{
    if (_type == Type::STRING)
    {
        value = _value_string;
    }
    else
    {
        throw std::logic_error(
            std::string("L’option ") + _longname + " n’est pas de type "
            + "chaîne de caractères. Il n’est pas possible de récupérer "
            + "la valeur comme chaîne de caractères."
        );
    }
}

void Option::setPassed(bool is_passed)
{
    _is_passed = is_passed;
}

void Option::setValue(std::string value)
{
    switch (_type)
    {
    case Type::STRING:
        _value_string = value;
        break;

    case Type::INT:
        _value_int = std::stoi(value);
        break;

    case Type::FLAG:
        throw std::logic_error(
            "Impossible d’affecter une valeur à une option booléenne."
        );
    }
}

int Option::getShortname() const
{
    return _shortname;
}

std::string Option::getLongname() const
{
    return _longname;
}

std::string Option::getDescription() const
{
    return _description;
}

Option::Type Option::getType() const
{
    return _type;
}

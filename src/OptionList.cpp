#include "OptionList.hpp"
#include <iomanip>

OptionList::OptionList(
    std::string name, std::string version,
    std::string authors
)
: _name(name), _version(version), _authors(authors)
{}

void OptionList::help(std::ostream& out)
{
    out << "Utilisation : " << _name << " [OPTION...]\n\n";

    for (auto &pair : _map)
    {
        // Affichage du nom de l'option et de son raccourci sur 30 colonnes
        Option opt = pair.second;

        std::string key =
            std::string("  -") + static_cast<char>(opt.getShortname()) +
            ", --" + opt.getLongname();

        out << std::left << std::setw(30) << key;

        // Si le nom de l’option est trop long, on met la description sur
        // une autre ligne
        if (key.size() >= 29)
        {
            out << std::endl << std::string(30, ' ');
        }

        out << opt.getDescription() << std::endl;
    }

    out << "\nAuteurs :\n" << _authors << std::endl;
}

void OptionList::version(std::ostream& out)
{
    out << _name << " " << _version << std::endl;
}

void OptionList::addOption(Option option)
{
    _map[option.getShortname()] = option;
}

const Option& OptionList::getOption(int shortname) const
{
    return _map.at(shortname);
}

bool OptionList::parse(unsigned int argc, char** argv)
{
    Option* current = NULL;

    for (std::size_t i = 1; i < argc; i++)
    {
        std::string text(argv[i]);

        if (text.size())
        {
            if (text[0] == '-')
            {
                // Analyse d’une nouvelle option
                if (text.size() == 1)
                {
                    // Erreur : une option sans nom
                    return false;
                }

                if (current != NULL)
                {
                    // Erreur : une option nécessitant une valeur sans valeur
                    return false;
                }

                current = NULL;

                if (text[1] == '-')
                {
                    // Recherche de l’option longue correspondante
                    std::string longname = text.substr(2);
                    bool found = false;

                    for (auto &pair : _map)
                    {
                        if (pair.second.getLongname() == longname)
                        {
                            found = true;
                            pair.second.setPassed(true);

                            if (pair.second.getType() != Option::Type::FLAG)
                            {
                                // Option attendant une valeur par la suite
                                current = &pair.second;
                            }

                            break;
                        }
                    }

                    if (!found)
                    {
                        // Erreur : option longue non trouvée
                        return false;
                    }
                }
                else
                {
                    if (text.size() > 2)
                    {
                        // Erreur : option courte de plus d’un caractère
                        return false;
                    }

                    // Recherche de l’option courte correspondante
                    if (_map.count(text[1]) == 0)
                    {
                        // Erreur : option courte non trouvée
                        return false;
                    }

                    // Indique qu’on a passé l’option
                    _map[text[1]].setPassed(true);

                    if (_map[text[1]].getType() != Option::Type::FLAG)
                    {
                        // Option attendant une valeur par la suite
                        current = &_map[text[1]];
                    }
                }
            }
            else
            {
                if (current == NULL)
                {
                    // Erreur : on essaye de mettre une valeur sans option
                    return false;
                }

                // Affectation d’une valeur à une option
                current->setValue(text);
            }
        }
    }

    return true;
}

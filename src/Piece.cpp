#include "Piece.hpp"
#include "tools.hpp"

Piece::Piece()
: _pivot_x(0), _pivot_y(0), _is_placed(false)
{}

Piece::Piece(
    Grid _shape,
    std::size_t _pivot_x,
    std::size_t _pivot_y
 )
: _shape(_shape), _pivot_x(_pivot_x), _pivot_y(_pivot_y), _is_placed(false)
{}

const Grid& Piece::getShape() const
{
    return _shape;
}

std::size_t Piece::getPivotX() const
{
    return _pivot_x;
}

std::size_t Piece::getPivotY() const
{
    return _pivot_y;
}

bool Piece::isPlaced() const
{
    return _is_placed;
}

void Piece::setPlaced(bool is_placed)
{
    _is_placed = is_placed;
}

void Piece::read(std::istream& in)
{
    std::string field;

    do
    {
        field = readField(in);
        ignoreChars(in, " \t");

        if (field.size())
        {
            if (field == "Placed")
            {
                in >> std::boolalpha >> _is_placed;
            }

            if (field == "Grid")
            {
                _shape.read(in);
            }

            if (field == "Pivot")
            {
                in >>_pivot_x;
                ignoreUntil(in, ",");
                in >>_pivot_y;
            }

            ignoreChars(in, " \t");

            if (in.get() != '\n')
            {
                throw std::runtime_error(
                    std::string("Caractères inattendus à la fin du champ '")
                    + field + "'."
                );
            }
        }
    }
    while (field.size());
}

void Piece::write(std::ostream& out) const
{
    if (_is_placed)
    {
        out << "Placed : " << std::boolalpha << _is_placed << std::endl;
    }

    out << "Pivot : " << _pivot_x << ", " << _pivot_y << std::endl;
    _shape.write(out);
}

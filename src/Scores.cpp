#include <fstream>
#include <cerrno>
#include <cstring>

#include "Scores.hpp"

// Constante contetant le chemin du fichier des scores
const char* score_filename = "../save/score_file.scores";

Scores::Scores()
{
    read();
}

void Scores::write() const
{
    std::ofstream score_file(score_filename, std::ios::out | std::ios::trunc);

    if (!score_file.is_open())
    {
        throw std::runtime_error(
            std::string("Le fichier de sauvegarde des scores n’a pas pu") +
            " être écrit : " + strerror(errno)
        );
    }

    for (std::size_t i = 0; i < _all_scores.size(); i++)
    {
        score_file << _all_scores[i].first << ":";
        score_file << _all_scores[i].second << std::endl;
    }
}

void Scores::read()
{
    std::ifstream score_file(score_filename, std::ios::in);

    if (!score_file.is_open())
    {
        if (errno != ENOENT)
        {
            throw std::runtime_error(
                std::string("Le fichier de sauvegarde des scores n’a pas pu") +
                " être lu : " + strerror(errno)
        );
        }
    }

    _all_scores.clear();

    std::string nom, score;

    while (
        std::getline(score_file, nom, ':') &&
        std::getline(score_file, score, '\n')
    )
    {
        _all_scores.push_back(std::make_pair(nom, std::stoi(score)));
    }

    std::sort(_all_scores.begin(), _all_scores.end(), sortScore);

    if (_all_scores.size() > 5)
    {
        _all_scores.resize(5);
    }
}

void Scores::addScore(ScorePair pair)
{
    if (_all_scores.size() < 5 || _all_scores.at(4).second < pair.second)
    {
        _all_scores.push_back(pair);
        std::sort(_all_scores.begin(), _all_scores.end(), sortScore);

        if (_all_scores.size() > 5)
        {
            _all_scores.resize(5);
        }
    }
}

const std::vector<ScorePair>& Scores::getPlaces() const
{
    return _all_scores;
}

bool sortScore(const ScorePair left, const ScorePair right)
{
    return left.second > right.second;
}

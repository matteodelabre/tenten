#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <ncurses.h>

#include "Color.hpp"
#include "TenTen.hpp"

TenTen::TenTen()
: _options(
      "tenten", "0.1",
      "  Rémi Cérès <remi.ceres@etu.umontpellier.fr>\n"
      "  Mattéo Delabre <matteo.delabre@etu.umontpellier.fr>\n"
      "  Jérémie Dautheribes <jeremie.dautheribes@etu.umontpellier.fr>\n"
      "  Camille Gosset <camille.gosset@etu.umontpellier.fr>"
  )
{
    _options.addOption(Option(
        'h', "help", "Afficher ces informations.", Option::Type::FLAG
    ));

    _options.addOption(Option(
        'v', "version", "Affiche la version du programme.", Option::Type::FLAG
    ));

    _options.addOption(Option(
        'l', "load", "Charge une partie sauvegardée.",
        Option::Type::STRING
    ));
}

int TenTen::start(int argc, char** argv)
{
    // Affichage de l’aide sur la sortie d’erreurs si mauvaise utilisation
    if (!_options.parse(argc, argv))
    {
        _options.help(std::cerr);
        return 1;
    }

    // Affichage de l’aide sur la sortie standard si demandé
    if (_options.getOption('h').isPassed())
    {
        _options.help();
        return 0;
    }

    // Affichage des informations de version si demandé
    if (_options.getOption('v').isPassed())
    {
        _options.version();
        return 0;
    }

    // Initialisation de la bibliothèque ncurses
    // (curseur, entrée clavier et couleurs)
    initscr();
    start_color();
    curs_set(0);
    noecho();
    raw();
    keypad(stdscr, true);
    initColorPairs();

    // Lecture des scores depuis le fichier de sauvegarde
    _scores.read();

    // Initialisation de la partie à la partie par défaut
    Game current_game(&_scores);

    if (_options.getOption('l').isPassed())
    {
        std::string filename;
        _options.getOption('l').getValue(filename);
        current_game.read(filename);
    }

    while (true)
    {
        switch (current_game.play())
        {
        case Game::Exit::QUIT:
            // Arrêt de la boucle principale et sortie du programme
            return 0;

        case Game::Exit::END:
            // Fin normale de la partie
            break;
        }

        current_game = Game(&_scores);
    }
}

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <ncurses.h>
#include <algorithm>

#include "TenTen.hpp"
#include "Grid.hpp"
#include "Color.hpp"

int main(int argc, char** argv)
{
    int exit_code;

    // Configuration de la langue
    setlocale(LC_ALL, "");

    // Initialisation de l’aléatoire
    std::srand(std::time(nullptr));

    try
    {
        // Lancement du jeu
        TenTen tenten;
        exit_code = tenten.start(argc, argv);
    }
    catch (const std::exception& exception)
    {
        // Erreur fatale lors de l’exécution du jeu :
        // on s’assure d’abord que le terminal est réinitialisé
        endwin();

        std::cerr << "Une erreur fatale s’est produite. ";
        std::cerr << exception.what() << std::endl;
        return EXIT_FAILURE;
    }

    // Réinitialisation du terminal
    endwin();

    return exit_code;
}

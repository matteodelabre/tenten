#include "tools.hpp"

std::string trimWhitespace(std::string str)
{
    std::size_t left = str.find_first_not_of("\t ");
    std::size_t right = str.find_last_not_of("\t ");

    if (left != std::string::npos && right != std::string::npos)
    {
        return str.substr(left, right - left + 1);
    }

    return str;
}

void ignoreChars(std::istream& in, std::string chars)
{
    // Lecture jusqu’au prochain caractère intéressant
    int read = in.get();

    while (chars.find(read) != std::string::npos && in)
    {
        read = in.get();
    }

    // Comme on a lu un caractère de trop, on revient d’un en arrière
    if (in)
    {
        in.unget();
    }
}

void ignoreUntil(std::istream& in, std::string chars)
{
    // Lecture jusqu’au prochain caractère intéressant
    int read = in.get();

    while (chars.find(read) == std::string::npos && in)
    {
        read = in.get();
    }
}

std::string readField(std::istream& in)
{
    std::string field;
    int read = in.get();

    // Lecture de la ligne jusqu’au prochain : ou bien la fin de la ligne
    while (read != ':' && read != '\n' && in)
    {
        field += read;
        read = in.get();
    }

    return trimWhitespace(field);
}
